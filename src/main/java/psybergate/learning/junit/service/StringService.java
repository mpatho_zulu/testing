package psybergate.learning.junit.service;

public class StringService {

	public String concate(String... strings) {
		String result = "";
		for(String string : strings) {
			result.concat(string);
		}
		return result;
	}
}
