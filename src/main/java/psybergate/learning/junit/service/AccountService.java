package psybergate.learning.junit.service;

import psybergate.learning.junit.entity.Account;
import psybergate.learning.junit.exception.AccountException;

public class AccountService {

	public void transact(Account fromAccount, Account toAccount, double amount) {
		if (!fromAccount.canWithdraw(amount)) {
			throw new AccountException("Insufficiant fund!");
		}
		fromAccount.withdral(amount);
		toAccount.deposite(amount);
	}
}
