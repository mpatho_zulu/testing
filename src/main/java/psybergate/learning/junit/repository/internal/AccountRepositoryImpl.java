package psybergate.learning.junit.repository.internal;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.List;

import psybergate.learning.junit.entity.Account;
import psybergate.learning.junit.exception.RepositoryException;
import psybergate.learning.junit.repository.AccountRepository;

public class AccountRepositoryImpl implements AccountRepository {

	private final String url;
	private final String user;
	private final String password;

	public AccountRepositoryImpl(String url, String user, String password) {
		this.url = url;
		this.user = user;
		this.password = password;
	}

	public AccountRepositoryImpl() {
		this("jdbc:postgresql://localhost/pps", "pps", "pps");
	}

	@Override
	public void save(Account fromAccount) {
		loadDriver();
		try (Connection connection = DriverManager.getConnection(url, user, password); Statement statement = connection.createStatement()) {
			String sql = String.format("do $$ begin IF EXISTS (SELECT * FROM account WHERE accountNumber = '%1$s') THEN "
					+ "UPDATE account SET balance = %2$f WHERE accountNumber = '%1$s'; "
					+ "ELSE INSERT INTO account(accountNumber, balance) VALUES ('%1$s', %2$f); "
					+ "END IF; end $$",
					fromAccount.getAccountNumber(), fromAccount.getBalance() );
			statement.execute(sql);
		} catch (SQLException e) {
			throw new RepositoryException("Transection failed!", e);
		}
	}

	@Override
	public List<Account> findAll() {
		loadDriver();
		String sql = "SELECT accountNumber, balance FROM account;";
		try (Connection connection = DriverManager.getConnection(url, user, password); Statement statement = connection.createStatement()) {
			ResultSet resultSet = statement.executeQuery(sql);
			return extactAccounts(resultSet);
		} catch (SQLException e) {
			throw new RepositoryException("Transection failed!", e);
		}
	}

	private static List<Account> extactAccounts(ResultSet resultSet) throws SQLException {
		List<Account> accounts = new ArrayList<>();
		while (resultSet.next()) {
			accounts.add(extractAccount(resultSet));
		}
		return accounts;
	}

	private static Account extractAccount(ResultSet resultSet) throws SQLException {
		String accountNumber = resultSet.getString(1);
		double balance = resultSet.getDouble(2);
		return new Account(accountNumber, balance);
	}

	private static void loadDriver() {
		try {
			Class.forName("org.postgresql.Driver");
		} catch (ClassNotFoundException e) {
			throw new Error(e);
		}
	}
}
