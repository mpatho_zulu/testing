package psybergate.learning.junit.repository;

import java.util.List;

import psybergate.learning.junit.entity.Account;

public interface AccountRepository {

	void save(Account fromAccount);

	List<Account> findAll();

}
